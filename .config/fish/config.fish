function fish_greeting
    /opt/local/bin/clear
    # xonsh
end

alias cd6 "cd ~/OneDrive/Documents/MatsumotoSchool/SixthGrade"
alias la "ls -A"
alias gs "git status"
alias mv "mv -i"
alias rm "rm -i"
alias nv "nvim"
alias bwin "brew install "

alias bwca "brew install --cask "
alias cp "cp -i"
alias df "df -h"
alias c "clear"
alias st "speedtest"
alias config "nvim ~/.config/fish/config.fish"
alias a "open -a "
alias fullpwd "pwd"
alias agenda "nvim ~/OneDrive/Documents/MatsumotoSchool/SixthGrade/Agenda/April.md"
alias gtop "~/gtop/bin/gtop"
alias exa "exa -alhSU --octal-permissions"
alias clear '/opt/local/bin/clear; echo; echo; seq 1 (tput cols) | sort -R | spark | lolcat; echo; echo'
alias cf '/opt/local/bin/clear'
#function arc
#    function fish_greeting
#        archey
#        echo "---------------------------------"
#    end
#end
#
#function shcolor
	#function fish_greeting
         #	./shell-color-scripts/colorscript.sh -r
         #	echo
         #	echo "          -------------------------------------------------------"
         #archey
	#end
#end

#function nfetch
#    function fish_greeting
#        neofetch
#    end
#end

function fish_user_key_bindings
    fish_vi_key_bindings
end

set EDITOR "kak ''"

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

function __history_previous_command
  switch (commandline -t)
 case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

bind ! __history_previous_command
bind '$' __history_previous_command_arguments
set PATH "/opt/local/bin:/opt/local/sbin:$PATH"

# set fish_color_normal brcyan
# set fish_color_autosuggestion '#7d7d7d'
# set fish_color_command brcyan
# set fish_color_error '#ff6c6b'
# set fish_color_param brcyan


alias df='df -h'

set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

starship init fish | source

set -x PKG_CONFIG_PATH /opt/local/lib/pkgconfig

export MICRO_TRUECOLOR=1

set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $HOME/Applications $fish_user_paths

# DOOM EMACS

#alias doomsync "~/.emacs.d/bin/doom sync"
#alias doomdoctor "~/.emacs.d/bin/doom doctor"
#alias doomconfig "emacsclient ~/.config/doom/config.org"
#alias doomupgrade "~/.emacs.d/bin/doom upgrade"
#alias doompurge "~/.emacs.d/bin/doom purge"

#zoxide init fish | source
#hexyl init fish | source


set NIX_LINK $HOME/.nix-profile/etc/profile.d/nix.sh
export NIX_PATH="nixpkgs=$HOME/.nix-defexpr/channels/nixos-21.05"
export PATH="$PATH:/Users/RaghavDixit/Library/Application Support/Coursier/bin"
set -gx PATH $PATH "$HOME/doom/bin"
set -gx PATH $PATH "$HOME/.nix-profile/bin/"
alias ed "ed -p ':' "
set -gx PATH $PATH "$HOME/dwm/"
set -gx PATH $PATH "$HOME/.cargo/bin/"
