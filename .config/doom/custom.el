(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elfeed-feeds '("https://odysee.com/$/rss/@DistroTube:2"))
 '(package-selected-packages
   '(fira-code-mode hasklig-mode 2048-game company-auctex auctex-latexmk nnreddit zweilight-theme gameoflife tern-auto-complete gdscript-mode minimap typing-game xelb writegood-mode wc-mode projectile pdf-tools org-superstar org-roam org-inline-pdf org-dashboard neotree doom-themes dashboard))
 '(smtpmail-smtp-server "disroot.org")
 '(smtpmail-smtp-service 587))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-comment-face ((t (:slant italic))))
 '(font-lock-keyword-face ((t (:slant italic)))))
(put 'customize-group 'disabled nil)
