#!/usr/bin/env bash
# set -euo pipefail

eval "$(starship init bash)"

export TERM="xterm-256color"                      # getting proper colors
export HISTCONTROL=ignoredups:erasedups           # no duplicate entries
export ALTERNATE_EDITOR=""                        # setting for emacsclient
export EDITOR="emacsclient -t -a ''"              # $EDITOR use Emacs in terminal
export VISUAL="emacsclient -c -a emacs"           # $VISUAL use Emacs in GUI mode


# Manpager: bat
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

# SET VI MODE

set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}


# ALIASES
alias cd6="cd ~/OneDrive/Documents/MatsumotoSchool/SixthGrade"
alias la="ls -A"
alias gs="git status"
alias mv="mv -i"
alias rm="rm -i"
alias nv="nvim"
alias bwin="brew install "
alias ls="exa -alhSU --octal-permissions --group-directories-first"
alias bwca="brew install --cask "
alias cp="cp -i"
alias df="df -h"
alias c="clear"
alias st="speedtest"
alias config="nvim ~/.config/fish/config.fish"
alias a="open -a "
alias fullpwd="pwd"
alias agenda="nvim ~/OneDrive/Documents/MatsumotoSchool/SixthGrade/Agenda/April.md"
alias gtop="~/gtop/bin/gtop"
alias emacs="open -a Emacs.app"
alias emacsterm="emacs "
alias exa="exa -alhSU --octal-permissions"
alias clear='/opt/local/bin/clear; echo; echo; seq 1 (tput cols) | sort -R | spark | lolcat; echo; echo'
alias cf='/opt/local/bin/clear'
alias vim="nvim"
alias em="/usr/bin/emacs -nw"
alias emacs="emacsclient -c -a 'emacs'"
alias doomsync="~/.emacs.d/bin/doom sync"
alias doomdoctor="~/.emacs.d/bin/doom doctor"
alias doomupgrade="~/.emacs.d/bin/doom upgrade"
alias doompurge="~/.emacs.d/bin/doom purge"
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias stat='git status'  # 'status' is protected name so using 'stat' instead
alias tag='git tag'
alias newtag='git tag -a'
alias rr='curl -s -L https://raw.githubusercontent.com/keroserene/rickrollrc/master/roll.sh | bash'
. "$HOME/.cargo/env"

source "$HOME/.nix-profile/etc/profile.d/nix.sh"
export NIX_PATH="nixpkgs=$HOME/.nix-defexpr/channels/nixos-21.05"
export PATH=/Users/RaghavDixit/.nix-profile/bin:/Users/RaghavDixit/.cargo/bin:/Users/RaghavDixit/.local/bin:/Users/RaghavDixit/Applications:/opt/local/bin:/opt/local/sbin:/opt/local/bin:/opt/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/Apple/usr/bin:/usr/local/share/dotnet:~/.dotnet/tools:/Library/Frameworks/Mono.framework/Versions/Current/Commands:/Users/RaghavDixit/Library/Application Support/Coursier/bin
export PATH=$HOME/.emacs.d/bin:$PATH
