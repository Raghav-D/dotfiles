import os;
import subprocess;
from qutebrowser.api import interceptor;
import dracula.draw

config.set("colors.webpage.darkmode.enabled", True);
config.bind(",ht", "set colors.webpage.darkmode.enabled false")

# Load existing settings made via :set
config.load_autoconfig()


config.bind(',hap', 'config-cycle content.user_stylesheets ~/.files/solarized-everything-css/css/apprentice/apprentice-all-sites.css ""')
config.bind(',hdr', 'config-cycle content.user_stylesheets ~/.files/solarized-everything-css/css/darculized/darculized-all-sites.css ""')
config.bind(',hgr', 'config-cycle content.user_stylesheets ~/.files/solarized-everything-css/css/gruvbox/gruvbox-all-sites.css ""')
config.bind(',hsd', 'config-cycle content.user_stylesheets ~/.files/solarized-everything-css/css/solarized-dark/solarized-dark-all-sites.css ""')
config.bind(',hsl', 'config-cycle content.user_stylesheets ~/.files/solarized-everything-css/css/solarized-light/solarized-light-all-sites.css ""')

# config.source('nord-qutebrowser.py')

config.set('content.javascript.enabled', True, 'chrome-devtools://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'devtools://*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'chrome://*/*')

# Enable JavaScript.
# Type: Bool
config.set('content.javascript.enabled', True, 'qute://*/*')

# Which cookies to accept. With QtWebEngine, this setting also controls
# other features with tracking capabilities similar to those of cookies;
# including IndexedDB, DOM storage, filesystem API, service workers, and
# AppCache. Note that with QtWebKit, only `all` and `never` are
# supported as per-domain values. Setting `no-3rdparty` or `no-
# unknown-3rdparty` per-domain on QtWebKit will have the same effect as
# `all`. If this setting is used with URL patterns, the pattern gets
# applied to the origin/first party URL of the page making the request,
# not the request URL.
# Type: String
# Valid values:
#   - all: Accept all cookies.
#   - no-3rdparty: Accept cookies from the same origin only. This is known to break some sites, such as GMail.
#   - no-unknown-3rdparty: Accept cookies from the same origin only, unless a cookie is already set for the domain. On QtWebEngine, this is the same as no-3rdparty.
#   - never: Don't accept cookies at all.
config.set('content.cookies.accept', 'all', 'chrome-devtools://*')

# Which cookies to accept. With QtWebEngine, this setting also controls
# other features with tracking capabilities similar to those of cookies;
# including IndexedDB, DOM storage, filesystem API, service workers, and
# AppCache. Note that with QtWebKit, only `all` and `never` are
# supported as per-domain values. Setting `no-3rdparty` or `no-
# unknown-3rdparty` per-domain on QtWebKit will have the same effect as
# `all`. If this setting is used with URL patterns, the pattern gets
# applied to the origin/first party URL of the page making the request,
# not the request URL.
# Type: String
# Valid values:
#   - all: Accept all cookies.
#   - no-3rdparty: Accept cookies from the same origin only. This is known to break some sites, such as GMail.
#   - no-unknown-3rdparty: Accept cookies from the same origin only, unless a cookie is already set for the domain. On QtWebEngine, this is the same as no-3rdparty.
#   - never: Don't accept cookies at all.
config.set('content.cookies.accept', 'all', 'devtools://*')

# User agent to send.  The following placeholders are defined:  *
# `{os_info}`: Something like "X11; Linux x86_64". * `{webkit_version}`:
# The underlying WebKit version (set to a fixed value   with
# QtWebEngine). * `{qt_key}`: "Qt" for QtWebKit, "QtWebEngine" for
# QtWebEngine. * `{qt_version}`: The underlying Qt version. *
# `{upstream_browser_key}`: "Version" for QtWebKit, "Chrome" for
# QtWebEngine. * `{upstream_browser_version}`: The corresponding
# Safari/Chrome version. * `{qutebrowser_version}`: The currently
# running qutebrowser version.  The default value is equal to the
# unchanged user agent of QtWebKit/QtWebEngine.  Note that the value
# read from JavaScript is always the global value. With QtWebEngine
# between 5.12 and 5.14 (inclusive), changing the value exposed to
# JavaScript requires a restart.
# Type: FormatString
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}', 'https://web.whatsapp.com/')

# User agent to send.  The following placeholders are defined:  *
# `{os_info}`: Something like "X11; Linux x86_64". * `{webkit_version}`:
# The underlying WebKit version (set to a fixed value   with
# QtWebEngine). * `{qt_key}`: "Qt" for QtWebKit, "QtWebEngine" for
# QtWebEngine. * `{qt_version}`: The underlying Qt version. *
# `{upstream_browser_key}`: "Version" for QtWebKit, "Chrome" for
# QtWebEngine. * `{upstream_browser_version}`: The corresponding
# Safari/Chrome version. * `{qutebrowser_version}`: The currently
# running qutebrowser version.  The default value is equal to the
# unchanged user agent of QtWebKit/QtWebEngine.  Note that the value
# read from JavaScript is always the global value. With QtWebEngine
# between 5.12 and 5.14 (inclusive), changing the value exposed to
# JavaScript requires a restart.
# Type: FormatString
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version} Edg/{upstream_browser_version}', 'https://accounts.google.com/*')

# User agent to send.  The following placeholders are defined:  *
# `{os_info}`: Something like "X11; Linux x86_64". * `{webkit_version}`:
# The underlying WebKit version (set to a fixed value   with
# QtWebEngine). * `{qt_key}`: "Qt" for QtWebKit, "QtWebEngine" for
# QtWebEngine. * `{qt_version}`: The underlying Qt version. *
# `{upstream_browser_key}`: "Version" for QtWebKit, "Chrome" for
# QtWebEngine. * `{upstream_browser_version}`: The corresponding
# Safari/Chrome version. * `{qutebrowser_version}`: The currently
# running qutebrowser version.  The default value is equal to the
# unchanged user agent of QtWebKit/QtWebEngine.  Note that the value
# read from JavaScript is always the global value. With QtWebEngine
# between 5.12 and 5.14 (inclusive), changing the value exposed to
# JavaScript requires a restart.
# Type: FormatString
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99 Safari/537.36', 'https://*.slack.com/*')

# Load images automatically in web pages.
# Type: Bool
config.set('content.images', True, 'chrome-devtools://*')

# Load images automatically in web pages.
# Type: Bool
config.set('content.images', True, 'devtools://*')

# Enable JavaScript.
# Type: Bool


# Allow websites to show notifications.
# Type: BoolAsk
# Valid values:
#   - true
#   - false
#   - ask

config.bind(",ly", "hint links spawn st -e lynx {hint-url}")

config.bind(",xt", "config-cycle tabs.show always switching")
config.bind(",xx", "config-cycle statusbar.show always never;; config-cycle tabs.show always switching")
config.bind(",xb", "config-cycle statusbar.show always never")
config.bind(",jn", "set content.javascript.enabled false ;; reload")
config.bind(",jy", "set content.javascript.enabled true ;; reload")

config.bind(",st", "set statusbar.position top")
config.bind(",sb", "set statusbar.position bottom")

config.bind(",tt", "set tabs.position top")
config.bind(",tl", "set tabs.position left")
config.bind(",tr", "set tabs.position right")
config.bind(",tb", "set tabs.position bottom")

c.url.default_page = "file:/Users/RaghavDixit/.qutebrowser/startpage.html"

c.url.searchengines = {
    'DEFAULT': 'https://search.brave.com/search?q={}&source=web',
    ':b': 'https://search.brave.com/search?q={}&source=web',
    ':d': 'https://duckduckgo.com/?q={}',
    ':g': 'https://www.google.com/search?q={}',
    ':rs': 'https://docs.rs/releases/search?query={}',
    ':r': 'https://www.reddit.com/r/{}',
    ':ub': 'https://www.urbandictionary.com/define.php?term={}',
    ':w': 'https://en.wikipedia.org/wiki/{}',
    ':y': 'https://www.youtube.com/results?search_query={}',
    ':gh': 'https://github.com/search?q=user%3ARaghav-Dixit+{}',
    ':gl': 'https://gitlab.com/search?group_id=&nav_source=navbar&project_id=&repository_ref=&search={}&snippets=false'
}

c.fonts.statusbar = '11pt "IBM Plex Mono"'
c.fonts.debug_console = '11pt "IBM Plex Mono"'
c.fonts.completion.entry = '11pt "IBM Plex Mono"'
c.fonts.default_family = '"IBM Plex Mono"'
