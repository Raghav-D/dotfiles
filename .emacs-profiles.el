(("default" . ((user-emacs-directory . "~/emacs-plus-plus/")))
("spacemacs". ((user-emacs-directory . "~/spacemacs/")))
("doom"     . ((user-emacs-directory . "~/doom/")))
("custom"   . ((user-emacs-directory . "~/custom-config/")))
("emacs-plus" . ((user-emacs-directory . "~/emacs-plus/"))))
